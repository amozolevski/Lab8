package automationTest;

import automation.Cat;
import automation.Dog;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ApplicationTest {

    Dog dog = new Dog();
    Cat cat = new Cat();

    @Test
    void dogMakeSoundTestCase(){
        Assert.assertEquals("Woof! Woof!", dog.makeSound());
    }

    @Test
    void catMakeSoundTestCase(){
        String result = cat.makeSound();
        Assert.assertTrue(result.equals("Meow! Meow!"), result + " its not Meow! Meow!");
    }

    @DataProvider(name = "dogDeepTest")
    private Object[][] createDogDeepTest(){
        return new String[][] {
                            {"woof! woof!", "Woof! Woof!"},
                            {"woof!woof!", "Woof! Woof!"},
                            {"Woof! Woof!", "Woof! Woof!"},
                            {"woofwoof", "Woof! Woof!"}
        };
    }

    @Test(dataProvider = "dogDeepTest")
    void dogDeepTestMakeSoundTestCase(String targetStr, String resultStr){
        Assert.assertEquals(targetStr, resultStr);
    }

    @DataProvider(name = "catDeepTest")
    private Object[][] createCatDeepTest(){
        return new Object[][]{
                            {"meow! meow!"},
                            {"meow!meow!"},
                            {"Meow! Meow!"},
                            {"meowmeow"}
        };
    }

    @Test(dataProvider = "catDeepTest")
    void catDeepTestMakeSoundTestCase(String targetStr){
        Assert.assertEquals(targetStr, cat.makeSound());
    }

}