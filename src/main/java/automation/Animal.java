package automation;

public interface Animal {
    String makeSound();
}
