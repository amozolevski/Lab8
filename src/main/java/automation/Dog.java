package automation;

public class Dog implements Animal{

    public String makeSound() {
        return "Woof! Woof!";
    }
}